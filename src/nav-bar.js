import {inject, bindable} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import './nav-var.css';
@inject(Router)
export class NavBar{
  @bindable router;
  constructor(router){
    this.router = router;
  }
  attached() {
    $(document).ready(function () {
      $('.right.menu.open').on("click", function (e) {
        e.preventDefault();
        $('.ui.vertical.menu').toggle();
      });

      $('.ui.dropdown').dropdown();
    });
  }
}
